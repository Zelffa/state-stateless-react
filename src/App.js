/*https://www.youtube.com/watch?v=nvHeB32ICDM*/
import React, { Component } from "react";
import logo from '../src/logo.svg';
import "./App.css"

class App extends React.Component{

  //FullState
  constructor(props) {
    super(props);
    this.state = {
      headerText: "Welcome shuy",
      contenText: "sadaskdjsajdasjkdjkasdjsjdsajkdasjkadsjk"
    }
    
  }

  render() {
    return (
      <div className="App">    
        {
          /*
          <h1>{this.state.headerText}</h1>
          <p>{this.state.contenText}</p>*/
        }

        <Header />
        <Content />
        
        
      </div>
    )
  }  

}

//lesStates
class Header extends Component {
  render(){
    return (
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2 id="header">Welcome shuy</h2>
      </div>
    );
  }
}

class Content extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          "id": 1,
          "name": "Shuy",
          "age": "22"
        }, {
          "id": 2,
          "name": "Caro",
          "age": "22"
        }, {
          "id": 3,
          "name": "Papa",
          "age": "12"
        }

      ]
    }
  }

  render() {
    return (
      <p className="App-intro">
        
        <h1>Just some content</h1>
        <p>asdasfdasjfsjfajasfjfaj</p>
        <div className="table">
        <table>
          <thead>
            <td>ID</td>
            <td>NAME</td>
            <td>AGE</td>
          </thead>
          <tbody>
            {this.state.data.map((person,i) => <TableRow key={i} data={person}/>)}
          </tbody>
          </table>
        </div>
      </p>
    );
  }
} 

class TableRow extends Component{
  render() {
    return (
      <tr>
        <td>{this.props.data.id}</td>
        <td>{this.props.data.name}</td>
        <td>{this.props.data.age}</td>
      </tr>
    )
  }
}

export default App;